## Installing in Drupal 7

If you are using drush (you should.), install and enable the module using

    drush en ebmg_integration

This should automatically download and enable it's dependencies: ctools, entityreference, entity, og

You then have to add fields to the "user" entity, to use them for the EbMG API.
These fields are:

 * First name
 * Last name
 * Salutation
 * Title
 * City
 * ZIP
 * Street
 * Country
 * Phone
 * Fax

The go to `admin/config/ebmg/settings` and map the fields to their XML counterparts.
Fill in the rest of the fields (EbMG-API URL, own API's username/password, etc.).

Now everything should work as expected.